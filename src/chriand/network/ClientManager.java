/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

/**
 *
 * @author Christoffer Andersson
 */
public final class ClientManager
{
    private static ConnectionClient connectionClient;
    private static IClientManager clientManager;

    public static void connect(String address, int port, IClientManager clientManager)
    {
        if(!isConnected())
        {
            ClientManager.clientManager = clientManager;
            ClientManager.connectionClient = clientManager.createConnection(address, port);
            ClientManager.connectionClient.start();
        }
    }

    public static IClientManager getClientManager()
    {
        return clientManager;
    }

    public static void update()
    {
        if(isConnected())
        {
            ClientManager.connectionClient.updateLogic();
        }
    }
    
    public static void shutdown()
    {
        if(connectionClient != null)
        {
            ClientManager.connectionClient.close();
            ClientManager.connectionClient = null;
        }
        ClientManager.clientManager = null;
    }

    public static boolean isConnected()
    {
        return connectionClient != null && connectionClient.isConnected();
    }

    public static boolean sendPacketTCP(NetworkData networkData, IPacketListener packetListener)
    {
        if(isConnected())
        {
            ClientManager.connectionClient.sendPacketTCP(networkData, packetListener);
            return true;
        }
        return false;
    }

    public static boolean answerRequestTCP(NetworkData networkData, NetworkData answer)
    {
        if(isConnected())
        {
            ClientManager.connectionClient.answerRequestTCP(networkData, answer);
            return true;
        }
        return false;
    }
}
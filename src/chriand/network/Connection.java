/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.Debugger;
import chriand.core.IRunnable;
import chriand.core.ThreadStarter;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoffer Andersson
 */
public class Connection implements IRunnable
{
    protected Socket socket;
    protected boolean running;
    protected PacketManager packetManager;

    public Connection(Socket socket) throws SocketException
    {
        this.socket = socket;
        this.packetManager = new PacketManager();
    }

    public void start()
    {
        if(!running)
        {
            ThreadStarter.startThreadFor(this, "Connection Receiver");
            this.running = true;
        }
    }

    protected void init()
    {

    }

    @Override
    public void run(int index)
    {
        this.init();
        Thread.currentThread().setName("Client " + socket.getRemoteSocketAddress().toString());
        while(running)
        {
            this.receiveTCP();
        }
        this.onConnectionLost();
        Debugger.success(this, "Listening thread killed");
    }

    public void updateLogic()
    {
        this.packetManager.updateLogic();
    }

    public void update()
    {
        try
        {
            this.packetManager.update(socket);
        }
        catch(IOException e)
        {
            Debugger.error(this, e);
            Debugger.error(this, "Failed to send networkData");
            this.running = false;
        }
    }

    protected void receiveTCP()
    {
        try
        {
            NetworkData networkData = NetworkManager.receiveTCP(socket);
            if(networkData.isRequest())
            {
                if(!onPacketRequestReceived(networkData))
                {
                    this.answerRequestTCP(networkData, new NetworkData(new byte[0], PacketTypeCore.Null));
                }
            }
            else
            {
                this.packetManager.onPacketReceivedTCP(networkData);
            }
        }
        catch(IOException e)
        {
            Debugger.error(this, e);
            Debugger.error(this, "Failed to receive networkData");
            this.running = false;
        }
    }

    public PacketManager getPacketManager()
    {
        return packetManager;
    }

    protected boolean onPacketRequestReceived(NetworkData networkData)
    {
        return false;
    }

    protected void onConnectionEstablished()
    {

    }

    protected void onConnectionLost()
    {

    }

    public void sendPacketTCP(NetworkData networkData, IPacketListener listener)
    {
        networkData.setRequest(true);
        this.packetManager.sendPacketTCP(networkData, listener);
    }

    public void answerRequestTCP(NetworkData networkData, NetworkData answer)
    {
        answer.setRequest(false);
        answer.setId(networkData.getId());
        this.packetManager.sendPacketTCP(answer, null);
    }
    
    public void close()
    {
        try
        {
            this.socket.close();
            this.running = false;
        }
        catch (IOException ex)
        {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
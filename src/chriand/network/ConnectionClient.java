/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.ThreadStarter;
import chriand.core.Debugger;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author Christoffer Andersson
 */
public class ConnectionClient extends Connection
{
    protected String address;
    protected int port;
    protected boolean reconnect;
    protected NetworkData initiationPacket;

    public ConnectionClient(String address, int port) throws SocketException
    {
        this(address, port, new NetworkData());
    }

    public ConnectionClient(String address, int port, NetworkData initiationPacket) throws SocketException
    {
        super(createSocket());
        this.address = address;
        this.port = port;
        this.initiationPacket = initiationPacket;
        this.reconnect = true;
    }
    
    protected static Socket createSocket() throws SocketException
    {
        Socket socket = new Socket();
        socket.setReuseAddress(true);
        socket.setTcpNoDelay(true);
        return socket;
    }

    @Override
    public void start()
    {
        if(!running)
        {
            ThreadStarter.startThreadFor(this, 2, "ConnectionClient Receiver", "ConnectionClient Sender");
            this.running = true;
        }
    }

    @Override
    protected void init()
    {
        super.init();
        try
        {
            Debugger.log(this, "Connecting to " + address + ":" + port);
            this.socket.connect(new InetSocketAddress(InetAddress.getByName(address), port), 3000);
            Debugger.log(this, "Successfully connected to " + address + ":" + port);
            this.initiationPacket.setRequest(true);
            NetworkManager.sendTCP(socket, initiationPacket);
            Debugger.success(this, "Initiation Packet Was Sent");
            this.running = true;
            this.onConnectionEstablished();
        }
        catch(UnknownHostException e)
        {
            Debugger.error(this, e);
            Debugger.error(this, "Failed to retrieve address from " + address + ":" + port);
            this.running = false;
            try
            {
                this.socket.close();
                this.socket = createSocket();
            }
            catch (IOException ex)
            {
                Debugger.error(this, ex);
            }
        }
        catch(IOException e)
        {
            Debugger.error(this, e);
            Debugger.error(this, "Failed to connect to " + address + ":" + port);
            this.running = false;
            try
            {
                this.socket.close();
                this.socket = createSocket();
            }
            catch (IOException ex)
            {
                Debugger.error(this, ex);
            }
        }
    }

    @Override
    public void run(int index)
    {
        boolean resetConnection = true;
        if(index == 0)
        {
            while(resetConnection)
            {
                this.init();
                while(running)
                {
                    this.receiveTCP();
                }
                this.onConnectionLost();
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ex)
                {
                    Debugger.error(this, ex);
                }
                resetConnection = reconnect;
            }
            Debugger.log(this, "Listening thread killed");
        }
        else if(index == 1)
        {
            Debugger.log(this, "Sender thread successfully started");
            while(resetConnection)
            {
                while(running)
                {
                    this.update();
                }
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ex)
                {
                    Debugger.error(this, ex);
                }
                resetConnection = reconnect;
            }
            Debugger.log(this, "Sender thread killed");
        }
    }

    public boolean isConnected()
    {
        return socket.isConnected() && running;
    }

    public void setReconnect(boolean reconnect)
    {
        this.reconnect = reconnect;
    }
    
    public boolean isReconnecting()
    {
        return reconnect;
    }
    
    @Override
    protected void onConnectionLost()
    {
        ClientManager.getClientManager().onConnectionLost();
    }

    @Override
    protected void onConnectionEstablished()
    {
        ClientManager.getClientManager().onConnectionEstablished();
    }

    @Override
    protected boolean onPacketRequestReceived(NetworkData networkData)
    {
        return ClientManager.getClientManager().onPacketRequestReceived(networkData);
    }
}
/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 *
 * @author Christoffer Andersson
 */
public class NetworkData
{
    protected byte[][] packets;
    protected int packetType;
    protected int id;
    protected boolean request;

    public NetworkData()
    {
        this(PacketTypeCore.Null);
    }
    
    public NetworkData(int packetType)
    {
        this(new byte[0], packetType);
    }

    public NetworkData(PacketDivider packet, int packetType)
    {
        this(packet.getBytes(), packetType);
    }

    public NetworkData(Packet packet, int packetType)
    {
        this(packet.getData(), packetType);
    }

    public NetworkData(Packet[] packets, int packetType)
    {
        this.packets = new byte[packets.length][];
        for (int i = 0; i < packets.length; i++)
        {
            this.packets[i] = packets[i].getData();
        }
    }

    public NetworkData(byte[] data, int packetType)
    {
        this.packets = new byte[1][];
        this.packets[0] = data;
        this.packetType = packetType;
    }

    public NetworkData(byte[][] packets, int packetType)
    {
        this.packets = packets;
        this.packetType = packetType;
    }
    
    public NetworkData(int packetType, byte[]... packets)
    {
        this.packets = packets;
        this.packetType = packetType;
    }

    private NetworkData(byte[] data, int packetType, int id, boolean request)
    {
        this.packetType = packetType;
        this.id = id;
        this.request = request;
        this.packets = new byte[ByteBuffer.wrap(data, 0, 4).getInt()][0];
        int headerSize = 4 * packets.length + 4;
        for (int i = 0; i < packets.length; i++)
        {
            int size = ByteBuffer.wrap(data, 4 + i * 4, 4).getInt();
            this.packets[i] = Arrays.copyOfRange(data, headerSize, headerSize + size);
            headerSize += size;
        }
    }

    public byte[] getBytes()
    {
        int size = 0;
        for (int i = 0; i < packets.length; i++)
        {
            size += packets[i].length;
        }
        byte[] data = new byte[size + packets.length * 4 + 4];
        byte[] packetSizeHeader = ByteBuffer.allocate(4).putInt(packets.length).array();
        for(int i = 0; i < packetSizeHeader.length; i++)
        {
            data[i] = packetSizeHeader[i];
        }

        int headerOffset = 4 + packets.length * 4;
        for (int i = 0; i < packets.length; i++)
        {
            byte[] packetSize = ByteBuffer.allocate(4).putInt(packets[i].length).array();
            for(int j = 0; j < packetSize.length; j++)
            {
                data[4 + i * 4 + j] = packetSize[j];
            }
            for(int j = 0; j < packets[i].length; j++)
            {
                data[headerOffset + j] = packets[i][j];
            }
            headerOffset += packets[i].length;
        }
        return data;
    }

    public byte[][] getData()
    {
        return packets;
    }

    public PacketDivider createPacketDivider()
    {
        return new PacketDivider(packets);
    }

    public void setPacketType(int packetType)
    {
        this.packetType = packetType;
    }

    public int getPacketType()
    {
        return packetType;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setRequest(boolean request)
    {
        this.request = request;
    }

    public boolean isRequest()
    {
        return request;
    }

    public static NetworkData createNetworkData(byte[] data, int packetType, int id, boolean request)
    {
        return new NetworkData(data, packetType, id, request);
    }

    @Override
    public String toString()
    {
        return "[PacketType:" + packetType + "][IsRequest:" + request + "][ID:" + id + "]";
    }
}
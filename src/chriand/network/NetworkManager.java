/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.Debugger;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;

/**
 *
 * @author Christoffer Andersson
 */
public final class NetworkManager
{
    private static void sendTCP(Socket socket, byte[] data, int packetType, int dataId, boolean request) throws IOException
    {
        byte[] bytes = ByteBuffer.allocate(data.length + 13).putInt(data.length).putInt(packetType).putInt(dataId).put((byte)(request ? 1 : 0)).put(data).array();
        socket.getOutputStream().write(bytes);
        socket.getOutputStream().flush();
    }

    public static void sendTCP(Socket socket, NetworkData networkData) throws IOException
    {
        Debugger.network(NetworkManager.class, "Sending " + networkData);
        NetworkManager.sendTCP(socket, networkData.getBytes(), networkData.getPacketType(), networkData.getId(), networkData.isRequest());
    }

    private static byte[] receiveTCP(Socket socket, int bytes) throws IOException
    {
        byte[] data = new byte[bytes];
        int bytesRead = 0;
        while(bytesRead < bytes)
        {
            bytesRead += socket.getInputStream().read(data, bytesRead, bytes - bytesRead);
        }
        return data;
    }

    public static NetworkData receiveTCP(Socket socket) throws IOException
    {
        int size = ByteBuffer.wrap(NetworkManager.receiveTCP(socket, 4)).getInt();
        int packetType = ByteBuffer.wrap(NetworkManager.receiveTCP(socket, 4)).getInt();
        int dataId = ByteBuffer.wrap(NetworkManager.receiveTCP(socket, 4)).getInt();
        byte request = ByteBuffer.wrap(NetworkManager.receiveTCP(socket, 1)).get();
        byte[] data = NetworkManager.receiveTCP(socket, size);
        NetworkData networkData = NetworkData.createNetworkData(data, packetType, dataId, request == 1);
        Debugger.network(NetworkManager.class, "Received " + networkData);
        return networkData;
    }

    public static void sendUDPPacket(DatagramSocket socket, byte[] data, InetAddress address, int port) throws IOException
    {
        DatagramPacket datagramPacket = new DatagramPacket(data, data.length, address, port);
        socket.send(datagramPacket);
    }

    private static void sendUDP(DatagramSocket socket, byte[] data, int packetType, InetAddress address, int port) throws IOException
    {
        byte[] bytes = ByteBuffer.allocate(data.length + 8).putInt(data.length).putInt(packetType).put(data).array();
        NetworkManager.sendUDPPacket(socket, bytes, address, port);
    }

    public static void sendUDP(DatagramSocket socket, NetworkData networkData, InetAddress address, int port) throws IOException
    {
        NetworkManager.sendUDP(socket, networkData.getBytes(), networkData.getPacketType(), address, port);
    }

    public static DatagramPacket receiveUDPPacket(DatagramSocket socket, byte[] buffer) throws IOException
    {
        DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
        socket.receive(datagramPacket);
        return datagramPacket;
    }

    public static NetworkData receiveUDP(DatagramSocket socket) throws IOException
    {
        DatagramPacket datagramPacket = NetworkManager.receiveUDPPacket(socket, new byte[1024]);
        ByteBuffer byteBuffer = ByteBuffer.wrap(datagramPacket.getData());
        int size = byteBuffer.getInt(0);
        int packetType = byteBuffer.getInt(4);
        int dataId = byteBuffer.getInt(8);
        byte request = byteBuffer.get(12);
        byte[] data = ByteBuffer.wrap(datagramPacket.getData(), 13, size).array();
        return NetworkData.createNetworkData(data, packetType, dataId, request == 1);
    }
}
/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import java.util.ArrayList;

/**
 *
 * @author Christoffer Andersson
 */
public class PacketDivider
{
    protected ArrayList<byte[]> packets;

    public PacketDivider()
    {
        this.packets = new ArrayList<byte[]>();
    }

    public PacketDivider(byte[][] bytes)
    {
        this.packets = new ArrayList<byte[]>();
        this.add(bytes);
    }

    public PacketDivider(ArrayList<byte[]> packets)
    {
        this.packets = new ArrayList<byte[]>();
        this.add(packets);
    }
    
    public PacketDivider(Packet[] packets)
    {
        this.packets = new ArrayList<byte[]>();
        this.add(packets);
    }

    public void add(byte[] data)
    {
        this.packets.add(data);
    }

    public void add(Packet packet)
    {
        this.packets.add(packet.getData());
    }

    public void add(Packet[] packets)
    {
        for (int i = 0; i < packets.length; i++)
        {
            this.packets.add(packets[i].getData());
        }
    }

    public void add(ArrayList<byte[]> packets)
    {
        this.packets.addAll(packets);
    }

    public void add(byte[][] packets)
    {
        for (int i = 0; i < packets.length; i++)
        {
            this.packets.add(packets[i]);
        }
    }

    public void add(PacketDivider divider)
    {
        this.packets.addAll(divider.packets);
    }

    public byte[] getPacket(int index)
    {
        return packets.get(index);
    }

    public byte[][] getBytes()
    {
        byte[][] bytes = new byte[packets.size()][];
        return packets.toArray(bytes);
    }

    public int getSize()
    {
        return packets.size();
    }
}
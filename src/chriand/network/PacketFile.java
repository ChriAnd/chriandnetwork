/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.FileData;

/**
 *
 * @author Christoffer Andersson
 */
public class PacketFile extends Packet
{
    protected FileData fileData;

    public PacketFile(FileData fileData)
    {
        this.fileData = fileData;
    }

    public PacketFile(byte[] bytes)
    {
        super(bytes);
    }

    @Override
    public void setData(byte[] bytes)
    {
        this.fileData = new FileData(bytes);
    }

    @Override
    public byte[] getData()
    {
        return fileData.getBytes();
    }

    public FileData getFileData()
    {
        return fileData;
    }
    
    public static PacketFile[] createPackets(FileData[] files)
    {
        PacketFile[] packetFiles = new PacketFile[files.length];
        for(int i = 0; i < files.length; i++)
        {
            packetFiles[i] = new PacketFile(files[i]);
        }
        return packetFiles;
    }
}
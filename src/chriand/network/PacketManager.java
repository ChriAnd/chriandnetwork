/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.serialization.DynamicArray;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.util.Pair;

/**
 *
 * @author Christoffer Andersson
 */
public class PacketManager
{
    private final DynamicArray<IPacketListener> listenersTCP;
    private final List<Pair<IPacketListener, NetworkData>> packetsTCP;
    private final List<Pair<IPacketListener, NetworkData>> answersTCP;

    public PacketManager()
    {
        this.listenersTCP = new DynamicArray<IPacketListener>();
        this.packetsTCP = Collections.synchronizedList(new ArrayList<Pair<IPacketListener, NetworkData>>());
        this.answersTCP = Collections.synchronizedList(new ArrayList<Pair<IPacketListener, NetworkData>>());
    }

    public void sendPacketTCP(NetworkData networkData, IPacketListener packetListener)
    {
        synchronized(packetsTCP)
        {
            this.packetsTCP.add(new Pair<IPacketListener, NetworkData>(packetListener, networkData));
        }
    }

    public void onPacketReceivedTCP(NetworkData networkData)
    {
        synchronized(listenersTCP)
        {
            synchronized(answersTCP)
            {
                if(networkData.getId() > -1 && networkData.getId() < listenersTCP.getLength())
                {
                    if(listenersTCP.get(networkData.getId()) != null)
                    {
                        this.answersTCP.add(new Pair<IPacketListener, NetworkData>(listenersTCP.get(networkData.getId()), networkData));
                        this.listenersTCP.remove(networkData.getId());
                    }
                }
            }
        }
    }

    public void updateLogic()
    {
        synchronized(answersTCP)
        {
            while(!answersTCP.isEmpty())
            {
                this.answersTCP.get(0).getKey().onPacketReceived(answersTCP.get(0).getValue());
                this.answersTCP.remove(0);
            }
        }
    }

    public void update(Socket socket) throws IOException
    {
        synchronized(packetsTCP)
        {
            while(!packetsTCP.isEmpty())
            {
                NetworkData networkData = packetsTCP.get(0).getValue();
                if(packetsTCP.get(0).getKey() != null)
                {
                    networkData.setId(listenersTCP.add(packetsTCP.get(0).getKey()));
                }
                this.packetsTCP.remove(0);
                NetworkManager.sendTCP(socket, networkData);
            }
        }
    }
}
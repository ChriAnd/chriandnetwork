/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.serialization.XMLNode;

/**
 *
 * @author Christoffer Andersson
 */
public class PacketXML extends Packet
{
    protected XMLNode xmlNode;
    
    public PacketXML()
    {
        this.xmlNode = createRootNode();
    }
    
    public PacketXML(XMLNode xmlNode)
    {
        this.xmlNode = xmlNode;
    }

    public PacketXML(byte[] bytes)
    {
        super(bytes);
    }

    public PacketXML(Object object, String tag)
    {
        this.xmlNode = new XMLNode("Root", "");
        this.xmlNode.add(object, tag);
    }

    public XMLNode getXML()
    {
        return xmlNode;
    }

    @Override
    public void setData(byte[] bytes)
    {
        this.xmlNode = XMLNode.fromString(new String(bytes));
    }

    @Override
    public byte[] getData()
    {
        return xmlNode.toDataString().getBytes();
    }
    
    public static XMLNode createRootNode()
    {
        return new XMLNode("Root", "");
    }
}
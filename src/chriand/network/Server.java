/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network;

import chriand.core.ThreadStarter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import chriand.core.Debugger;
import chriand.core.IRunnable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoffer Andersson
 */
public class Server implements IRunnable, IServerAccess
{
    protected List<ConnectionServer> connectionsServer;
    protected ServerSocket socket;
    protected IServer listener;
    protected boolean running;
    protected String address;
    protected int port;
    protected int maxQue;

    public Server(IServer listener)
    {
        this.connectionsServer = Collections.synchronizedList(new ArrayList<ConnectionServer>());
        this.listener = listener;
    }

    protected synchronized void removeConnection(ConnectionServer connectionServer)
    {
        synchronized(connectionsServer)
        {
            this.connectionsServer.remove(connectionServer);
        }
        synchronized(listener)
        {
            this.listener.onConnectionLost(connectionServer);
        }
    }

    protected synchronized void addConnection(ConnectionServer connectionServer)
    {
        synchronized(connectionsServer)
        {
            this.connectionsServer.add(connectionServer);
        }
    }

    public synchronized List<ConnectionServer> getConnections()
    {
        return connectionsServer;
    }

    public boolean isRunning()
    {
        return running;
    }

    public void bind(String address, int port, int maxQue) throws IOException
    {
        if(!running)
        {
            this.port = port;
            this.maxQue = maxQue;
            this.running = true;
            this.address = address;
            ThreadStarter.startThreadFor(this, 2, "Server Listener", "Server Sender");
        }
    }

    public void update()
    {
        synchronized(connectionsServer)
        {
            for(int i = connectionsServer.size() - 1; i >= 0; i--)
            {
                connectionsServer.get(i).updateLogic();
            }
        }
    }

    @Override
    public void run(int index)
    {
        if(index == 0)
        {
            Thread.currentThread().setName("Server Listener");
            this.bind();
            while(running)
            {
                try
                {
                    Socket clientSocket = socket.accept();
                    Debugger.log(this, "New Client Accepted, Waiting For NetworkData...");
                    ConnectionServer connectionServer = listener.createConnection(clientSocket, this, NetworkManager.receiveTCP(clientSocket));
                    if(connectionServer != null)
                    {
                        this.addConnection(connectionServer);
                        connectionServer.start();
                        Debugger.log(this, "Clients Connected " + connectionsServer.size());   
                    }
                    else
                    {
                        Debugger.log(this, "Client Faild To Connect");   
                        clientSocket.close();
                    }
                }
                catch(IOException e)
                {
                    if(running)
                    {
                        Debugger.error(this, e);
                        Debugger.error(this, "Failed to accept client");
                    }
                }
            }
            Debugger.success(this, "Successfully closed listener Thread");
        }
        else if(index == 1)
        {
            Thread.currentThread().setName("Server Sender");
            Debugger.success(this, "Successfully started sender Thread");
            while(running)
            {
                synchronized(connectionsServer)
                {
                    for(int i = connectionsServer.size() - 1; i >= 0; i--)
                    {
                        connectionsServer.get(i).update();
                    }
                }
            }
            Debugger.success(this, "Successfully closed sender Thread");
        }
    }

    protected void bind()
    {
        try
        {
            Debugger.important(this, "Binding socket to " + address + ":" + port);
            this.socket = new ServerSocket(port, maxQue, InetAddress.getByName(address));
            Debugger.success(this, "Successfully bound to " + address + ":" + port);
        }
        catch(IOException e)
        {
            Debugger.error(this, e);
            Debugger.error(this, "Failed to bind to " + address + ":" + port);
            this.running = false;
        }
    }
    
    public void shutdown()
    {
        try
        {
            this.running = false;
            for(int i = connectionsServer.size() - 1; i >= 0; i--)
            {
                connectionsServer.get(i).close();
            }
            this.socket.close();
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Server getServer()
    {
        return this;
    }
}
/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.network.lan;

import chriand.core.ThreadStarter;
import chriand.core.Debugger;
import chriand.core.IRunnable;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christoffer Andersson
 */
public final class LocalAreaNetwork implements IRunnable
{
    private static LocalAreaNetwork localAreaNetwork;
    private ArrayList<String> serversFound;
    private DatagramSocket socket;
    private IServerLANListener serverListener;
    private IClientLANListener clientListener;
    private final String password;
    private String userdata;
    private final int port;
    private final int sleep;
    private boolean exit;
    
    private LocalAreaNetwork(IClientLANListener clientListener, String password, String userdata, int port, int sleep)
    {
        this.password = password;
        this.port = port;
        this.sleep = sleep;
        this.userdata = userdata;
        this.clientListener = clientListener;
        if(userdata == null)
        {
            this.userdata = "";
        }
    }
    
    private LocalAreaNetwork(IServerLANListener serverListener, String password, String userdata, int port, int sleep)
    {
        this.serverListener = serverListener;
        this.password = password;
        this.port = port;
        this.sleep = sleep;
        this.userdata = userdata;
        if(userdata == null)
        {
            this.userdata = "";
        }
        this.serversFound = new ArrayList<String>();
    }
    
    public void shutdown()
    {
        this.exit = true;
        this.socket.close();
        LocalAreaNetwork.localAreaNetwork = null;
    }

    @Override
    public void run(int index)
    {
        try
        {
            Thread.sleep(sleep);
        }
        catch (InterruptedException ex)
        {
            Logger.getLogger(LocalAreaNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(serverListener == null)
        {
            Debugger.success(this, "Starting Server LAN Host");
            this.respondToClients();
            this.socket.close();
            LocalAreaNetwork.localAreaNetwork = null;
            Debugger.success(this, "Exiting Server LAN Host");
        }
        else
        {
            Debugger.success(this, "Starting Client LAN Host");
            if(broadcastClientInfo())
            {
                while (true)
                {                
                    if(!listenForServer())
                    {
                        break;
                    }
                }
            }
            else
            {
                Debugger.success(this, "Failed To Broadcast Client Info");
            }
            this.socket.close();
            LocalAreaNetwork.localAreaNetwork = null;
            if(!exit)
            {
                this.serverListener.onServerNotFound();
            }
            Debugger.success(this, "Exiting Client LAN Host");
        }
        this.serverListener = null;
        this.clientListener = null;
    }
    
    public static void bindServer(String password, int port, int sleep)
    {
        LocalAreaNetwork.bindServer(null, password, "", port, sleep);
    }
    
    public static void bindServer(String password, String userdata, int port, int sleep)
    {
        LocalAreaNetwork.bindServer(null, password, userdata, port, sleep);
    }
    
    public static void bindServer(IClientLANListener clientListener, String password, String userdata, int port, int sleep)
    {
        if(localAreaNetwork == null)
        {
            LocalAreaNetwork.localAreaNetwork = new LocalAreaNetwork(clientListener, password, userdata, port, sleep);
            ThreadStarter.startThreadFor(localAreaNetwork, "LAN Server");
        }
    }
    
    public static void bindClient(IServerLANListener serverListener, String password, int port, int sleep)
    {
        LocalAreaNetwork.bindClient(serverListener, password, "", port, sleep);
    }
    
    public static void bindClient(IServerLANListener serverListener, String password, String userdata, int port, int sleep)
    {
        if(localAreaNetwork == null)
        {
            LocalAreaNetwork.localAreaNetwork = new LocalAreaNetwork(serverListener, password, userdata, port, sleep);
            ThreadStarter.startThreadFor(localAreaNetwork, "LAN CLient");
        }
    }
    
    public static LocalAreaNetwork getInstance()
    {
        return localAreaNetwork;
    }
    
    private void respondToClients()
    {
        try
        {
            this.socket = new DatagramSocket(port, InetAddress.getByName("0.0.0.0"));
            this.socket.setBroadcast(true);
            
            while (localAreaNetwork != null)
            {
                try
                {
                    byte[] buffer = new byte[512];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    this.socket.receive(packet);
                    Debugger.success(this, "Received LAN Packet From [" + packet.getAddress().getHostAddress() + "]");
                    String dataString = new String(packet.getData()).trim();
                    String fullPassword = "CLIENT_" + password;
                    if (dataString.startsWith(fullPassword))
                    {
                        if(clientListener == null || clientListener.onClientFound(packet.getAddress().getHostAddress(), dataString.substring(fullPassword.length())))
                        {
                            byte[] data = ("SERVER_" + password + this.userdata).getBytes();
                            DatagramPacket sendPacket = new DatagramPacket(data, data.length, packet.getAddress(), packet.getPort());
                            this.socket.send(sendPacket);
                        }                     
                    }
                } 
                catch (IOException ex)
                {
                    Debugger.error(this, ex);
                }
            }
        }
        catch (SocketException ex)
        {
            Debugger.error(this, ex);
        }
        catch (UnknownHostException ex)
        {
            Debugger.error(this, ex);
        }
    }
    
    private boolean broadcastClientInfo()
    {
        try
        {
            this.socket = new DatagramSocket();
            this.socket.setBroadcast(true);
            this.socket.setSoTimeout(10000);
            byte[] data = ("CLIENT_" + password + userdata).getBytes();
            DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName("255.255.255.255"), port);
            this.socket.send(packet);
            Debugger.success(this, "Sending LAN Packet To [255.255.255.255]");
            this.extendBroadcast(data);
            return true;
        }
        catch (Exception ex)
        {
            Debugger.error(this, ex);
        }
        return false;
    }
    
    private void extendBroadcast(byte[] data)
    {
        try
        {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements())
            {
                NetworkInterface networkInterface = (NetworkInterface) networkInterfaces.nextElement();
                if (networkInterface.isLoopback() || !networkInterface.isUp())
                {
                    continue;
                }

                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses())
                {
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    if (broadcast != null)
                    {
                        this.socket.send(new DatagramPacket(data, data.length, broadcast, port));
                        Debugger.success(this, "Sending LAN Packet To [" + broadcast.getHostAddress() + "] On Interface [" + networkInterface.getDisplayName() + "]");
                    }
                }
            }
        }
        catch (SocketException ex)
        {
            Logger.getLogger(LocalAreaNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex)
        {
            Logger.getLogger(LocalAreaNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean listenForServer()
    {
        try
        {
            byte[] buffer = new byte[512];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            this.socket.receive(packet);
            String dataString = new String(packet.getData()).trim();
            String fullPassword = "SERVER_" + password;
            String address = packet.getAddress().getHostAddress();
            if(!serversFound.contains(address))
            {
                if(dataString.startsWith(fullPassword))
                {
                    if(serverListener.onServerFound(address, dataString.substring(fullPassword.length())))
                    {
                        this.serversFound.add(address);
                        return true;
                    }
                }
            }
            else if(!socket.isClosed())
            {
                return listenForServer();
            }
        }      
        catch (IOException ex)
        {
            
        }
        return false;
    }
}